# Views Exposed Filter Switch

This module creates an exposed filter form type for Views that combines exposed
filter items into a single, switchable element. Only the element currently
selected by the switch will be used for search upon submission of the exposed
form.

## To use:

1. Enable this module like any other module.
2. Add exposed filters to a view.
3. Under "Exposed form style" in the Views UI for that view, click the currently
selected plugin title (such as "Basic") to select a different form style.
4. In the resulting window, select "Basic with filter search" from the options.
5. Click "Apply".

Notes:

* The Settings form for this plugin comes directly from the "Basic" plugin.
* If multiple exposed forms are to be used on the same page, a patch to core is
  required for #states to act as desired:
  See #7 in https://www.drupal.org/node/1831560
