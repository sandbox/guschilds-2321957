<?php

/**
 * @file
 * Definition of views_plugin_exposed_form_basic_filter_switch.
 */

/**
 * Exposed form plugin that combines the basic exposed form into one element.
 *
 * @ingroup views_exposed_form_plugins
 */
class views_plugin_exposed_form_basic_filter_switch extends views_plugin_exposed_form_basic {

  /**
   * Alters the exposed filter form to combine the items and add the switch.
   *
   * @param array $form
   *   The exposed form to be altered.
   * @param array $form_state
   *   The current state of the form.
   */
  function exposed_form_alter(&$form, &$form_state) {
    parent::exposed_form_alter($form, $form_state);

    // The switch is given a unique ID to avoid conflicts when multiple exposed
    // forms exist on the same page. This ID persists through submissions when
    // the initial form and resulting view are on different pages.
    if (!empty($_GET)) {
      foreach ($_GET as $key => $value) {
        if (strpos($key, 'basic-filter-switch') !== FALSE) {
          $switch_id = $key;
          break;
        }
      }
    }
    if (!isset($switch_id)) {
      $switch_id = drupal_html_id('basic_filter_switch');
    }
    $form['#views_exposed_filter_switch'] = array('switch_id' => $switch_id);

    // Construct the options for the switch item from existing items.
    $form_items = array();
    $exclude = array('hidden', 'token', 'submit');
    foreach (element_children($form) as $key) {
      if (empty($form[$key]['#type'])) {
        // Some elements, such as dates, don't follow the typical structure.
        foreach (element_children($form[$key]) as $child_key) {
          if (!empty($form[$key][$child_key]['#type'])) {
            $form[$key]['#type'] = 'container';
          }
        }
      }

      if (!empty($form[$key]['#type']) && !in_array($form[$key]['#type'], $exclude)) {
        // The array item's value (the label) is added in the next step.
        $form_items[$key] = '';

        // The element is given an ID unique to this form. See comments above.
        $form[$key]['#id'] = drupal_html_id($key);

        // Alter visibility of this form item based on the value of the switch.
        if (empty($form[$key]['#states'])) {
          $form[$key]['#states'] = array();
        }
        $form[$key]['#states']['visible'][] = array(
          ':input[name="' . $switch_id . '"]' => array('value' => $key),
        );
      }
    }

    foreach ($form['#info'] as $filter_key => $filter_info) {
      // Assign the label for each array item.
      $key = $filter_info['value'];
      if (array_key_exists($key, $form_items)) {
        $form_items[$key] = $filter_info['label'];
      }

      // Clear the item's label so it doesn't appear on the page.
      // TODO: May be better for accessibility if this was done with CSS?
      $form['#info'][$filter_key]['label'] = '';
    }

    $form[$switch_id] = array(
      '#type' => 'select',
      '#options' => $form_items,
      '#weight' => -100,
    );

    // Provide info for the filter switch item.
    $switch_info['filter-' . $switch_id] = array('value' => $switch_id);
    $form['#info'] = $switch_info + $form['#info'];
  }

  /**
   * Manipulates values upon submission so only the active item is read.
   *
   * @param array $form
   *   The exposed form to be altered.
   * @param array $form_state
   *   The current state of the form.
   */
  function exposed_form_validate(&$form, &$form_state) {
    // Non-item elements to ignore. Similar to views_exposed_form_submit().
    $exclude = array('submit', 'form_build_id', 'form_token', 'form_id', '');
    $switch_id = $form['#views_exposed_filter_switch']['switch_id'];

    // Reads the value of the switch item and sets the value of any item that
    // isn't currently selected to their default. This prevents all filter
    // items being read from.
    $switch_value = $form_state['values'][$switch_id];
    foreach ($form_state['values'] as $key => $value) {
      if (!in_array($key, $exclude) && $switch_value != $key) {
        $default_value = !empty($form[$key]['#default_value']) ? $form[$key]['#default_value'] : '';
        $form_state['values'][$key] = $default_value;
      }
    }

    parent::exposed_form_validate($form, $form_state, $exclude);
  }
}
